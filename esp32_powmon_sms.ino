/*
  This ESP32 application sends SMS upon events of power failure. It is intended 
  to be triggered by dry-contact connected to PINS 32 and/or 33.
  
  The code in this project is based on the project below.
  
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/esp32-sim800l-send-text-messages-sms/
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*/


// SIM card PIN (leave empty, if not defined)
const char simPIN[]   = "";

// To define SMS target pull-down PIN2 and reset board. Serial will ask you new SMS target.
// SMS_TARGET is provided as dialing from a regular phone.
// Ex.: +5511987654321 
// Ex.:   011987654321
// Ex.:      987654321
String _SMSTARGET;

// Define last power state
String _lstpow;
String _powmains;
String _powgener;

// Configure sleep time upon a trigger
#define GRACETIME   15

// Configure trigger PINs
#define PINCONFIG  2
#define PINMAINS  32
#define PINGENER  33

// Define access to persistent storage
#include <Preferences.h>
#define RW_MODE false
#define RO_MODE true
Preferences p;

// Configure TinyGSM library
#define TINY_GSM_MODEM_SIM800      // Modem is SIM800
#define TINY_GSM_RX_BUFFER   1024  // Set RX buffer to 1Kb

#include <Wire.h>
#include <TinyGsmClient.h>

// TTGO T-Call pins
#define MODEM_RST            5
#define MODEM_PWKEY          4
#define MODEM_POWER_ON       23
#define MODEM_TX             27
#define MODEM_RX             26
#define I2C_SDA              21
#define I2C_SCL              22

// Set serial for debug console (to Serial Monitor, default speed 115200)
#define SerialMon Serial

// Set serial for AT commands (to SIM800 module)
#define SerialAT  Serial1

// Define the serial console for debug prints, if needed
//#define DUMP_AT_COMMANDS

#ifdef DUMP_AT_COMMANDS
  #include <StreamDebugger.h>
  StreamDebugger debugger(SerialAT, SerialMon);
  TinyGsm modem(debugger);
#else
  TinyGsm modem(SerialAT);
#endif

#define IP5306_ADDR          0x75
#define IP5306_REG_SYS_CTL0  0x00

bool setPowerBoostKeepOn(int en){
  Wire.beginTransmission(IP5306_ADDR);
  Wire.write(IP5306_REG_SYS_CTL0);
  if (en) {
    Wire.write(0x37); // Set bit1: 1 enable 0 disable boost keep on
  } else {
    Wire.write(0x35); // 0x37 is default reg value
  }
  return Wire.endTransmission() == 0;
}

//
// END DEFINITIONS
// --------------------------------------------------------------------------------------


// --------------------------------------------------------------------------------------
// SETUP BLOCK
// 
void setup() {
  // Set trigger PIN
  pinMode(PINMAINS,  INPUT_PULLUP);
  pinMode(PINGENER,  INPUT_PULLUP);
  pinMode(PINCONFIG, INPUT_PULLUP);

  // Set modem reset, enable, power pins
  pinMode(MODEM_PWKEY, OUTPUT);
  pinMode(MODEM_RST, OUTPUT);
  pinMode(MODEM_POWER_ON, OUTPUT);
  digitalWrite(MODEM_PWKEY, LOW);
  digitalWrite(MODEM_RST, HIGH);
  digitalWrite(MODEM_POWER_ON, HIGH);
  
  // Set serial console baud rate
  SerialMon.begin(115200);
  delay(3000);
  
  // Application startup banner
  SerialMon.println("\n#\n-----------------------");
  SerialMon.println("POW MON APP has started\n");

  // Keep power when running from battery
  Wire.begin(I2C_SDA, I2C_SCL);
  bool isOk = setPowerBoostKeepOn(1);
  SerialMon.println(String("IP5306 KeepOn ") + (isOk ? "OK" : "FAIL"));
  
  if (digitalRead(PINCONFIG) == LOW) {
    SerialMon.println("Provide SMS destination number");
    while(SerialMon.available() == 0){   }
    _SMSTARGET = Serial.readString();
    SerialMon.println("You provided the number " + _SMSTARGET);
    p.begin("pow-status", RW_MODE);
    p.putString("_SMSTARGET", _SMSTARGET);
    p.end();
  } 

  // Set GSM module baud rate and UART pins
  SerialAT.begin(115200, SERIAL_8N1, MODEM_RX, MODEM_TX);
  delay(3000);

  // Restart SIM800 module, it takes quite some time
  // use modem.init() if you don't need the complete restart
  SerialMon.print("Init modem.......");
  modem.restart();
  // modem.init();
  Serial.println("[done]");

  // Unlock your SIM card with a PIN if needed
  if (strlen(simPIN) && modem.getSimStatus() != 3 ) { modem.simUnlock(simPIN); }

  // Load persistent configuration parameters
  p.begin("pow-status", RO_MODE);

  if (p.isKey("_SMSTARGET")) { 
    _SMSTARGET = p.getString("_SMSTARGET");
  } else {
    _SMSTARGET = "0";
  }

  if (! p.isKey("_lstpow")) {
    _lstpow = "OFFLINEOFFLINE";
  } else {
    _lstpow = p.getString("_lstpow");
    Serial.println("!");
    delay(700);
  }
  p.end();

  SerialMon.println(String(digitalRead(PINMAINS)) + String(digitalRead(PINGENER)));
  SerialMon.println("SMS Target: " + _SMSTARGET);
  SerialMon.println("Monitoring has started");
}

void PersistConfig(){
  // Persists last changed state
  _lstpow = _powmains + _powgener;
  p.begin("pow-status", RW_MODE);
  p.putString("_lstpow", _lstpow);
  p.end();
}

void TriggerSMS() {
  // Build SMS to be sent
  String smsMessage = "__Power Monitor__\nMAINS.... " + _powmains + "\nGENERATOR " + _powgener;
  SerialMon.println("Sending SMS to " + String(_SMSTARGET) +"\n");
  SerialMon.println(smsMessage);
  
  // Send alert SMS
  int cnt = 0;
  while (cnt < 3){
    if (modem.sendSMS(_SMSTARGET, smsMessage)){ 
      SerialMon.println("\nSMS has been sent");
      cnt = 3;
    } else {
      SerialMon.println("\nSMS failed to send");
      delay(3000);
    }
    cnt++;
  }
}

// --------------------------------------------------------------------------------------
// APPLICATION LOOP BLOCK
// 
void loop() {
  // Check PINs status
  if (digitalRead(PINMAINS) == LOW) { _powmains = "ONLINE"; } else { _powmains = "OFFLINE"; }
  if (digitalRead(PINGENER) == LOW) { _powgener = "ONLINE"; } else { _powgener = "OFFLINE"; }
  
  if (_lstpow != _powmains + _powgener) {
    SerialMon.println("Status has changed");
    TriggerSMS();
    PersistConfig();
    SerialMon.println("Sleeping " + String(GRACETIME) + "s to avoid flapping");
    delay(GRACETIME*1000);
    SerialMon.println("Waiting for events");
  }
  delay(100);
}

